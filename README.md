# prosv-api-client
> Client for API of Prosvescheniye distribution system.

## Installation

```sh
$ npm install --save prosv-api-client
```

## Usage

```js
var ProsvApiClient = require('prosv-api-client');

var client = ProsvApiClient('consumer', 'salt');

// Get the showcase
client
  .getShowcase()
  .then(function (showcase) {
    console.log(showcase);
  })
  .catch(function (err) {
    console.log('error', err);
  });
  
// Get item description
client
  .getItemDescription(11000)
  .then(function (item) {
    console.log(item);
  })
  .catch(function (err) {
    console.log('error', err);
  });
  
// Get item data
client
  .getItemData(11000)
  .then(function (book) {
    console.log(book);
  })
  .catch(function (err) {
    console.log('error', err);
  });
```

## Testing

```sh
gulp test
```

## Building

```sh
gulp babel
```

## License

 © [Denis Bezrukov](http://www.e-azbuka.ru/)


[npm-image]: https://badge.fury.io/js/prosv-api-client.svg
[npm-url]: https://npmjs.org/package/prosv-api-client
[travis-image]: https://travis-ci.org//prosv-api-client.svg?branch=master
[travis-url]: https://travis-ci.org//prosv-api-client
[daviddm-image]: https://david-dm.org//prosv-api-client.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//prosv-api-client
[coveralls-image]: https://coveralls.io/repos//prosv-api-client/badge.svg
[coveralls-url]: https://coveralls.io/r//prosv-api-client
