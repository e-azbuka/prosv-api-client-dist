'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _nodeRestClient = require('node-rest-client');

var _nodeRestClient2 = _interopRequireDefault(_nodeRestClient);

var _sha = require('sha1');

var _sha2 = _interopRequireDefault(_sha);

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

require('babel-polyfill');

var ProsvApiClient = function () {
  function ProsvApiClient(consumer, salt) {
    _classCallCheck(this, ProsvApiClient);

    if (!consumer) {
      throw new Error('Argument consumer is required');
    }
    if (!salt) {
      throw new Error('Argument salt is required');
    }
    var client = new _nodeRestClient2.default.Client();
    this._salt = salt;
    this._consumer = consumer;
    this._client = client;
    this._baseUrl = 'http://catalog.prosv.ru/api/v1/';
  }

  _createClass(ProsvApiClient, [{
    key: 'getShowcase',
    value: function () {
      var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this._makeRequest('showcase', {});

              case 2:
                return _context.abrupt('return', _context.sent);

              case 3:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getShowcase() {
        return _ref.apply(this, arguments);
      }

      return getShowcase;
    }()
  }, {
    key: 'getItemDescription',
    value: function () {
      var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(id) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this._makeRequest('item', { id: id });

              case 2:
                return _context2.abrupt('return', _context2.sent);

              case 3:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getItemDescription(_x) {
        return _ref2.apply(this, arguments);
      }

      return getItemDescription;
    }()
  }, {
    key: 'getItemData',
    value: function () {
      var _ref3 = _asyncToGenerator(regeneratorRuntime.mark(function _callee3(id) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this._makeRequest('contents', { id: id });

              case 2:
                return _context3.abrupt('return', _context3.sent);

              case 3:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function getItemData(_x2) {
        return _ref3.apply(this, arguments);
      }

      return getItemData;
    }()
  }, {
    key: '_makeRequest',
    value: function _makeRequest(action, payload) {
      var def = _q2.default.defer();
      var signature = this._getSignature(action, payload);
      var parameters = {
        action: action,
        consumer: this._consumer,
        signature: signature
      };
      var args = {
        data: payload,
        parameters: parameters
      };
      this._client.post(this._baseUrl + 'ebook', args, function (data) {
        if (data.error) return def.reject(data.error);
        return def.resolve(data.result);
      }).on('error', function (err) {
        def.reject(err);
      });
      return def.promise;
    }
  }, {
    key: '_getSignature',
    value: function _getSignature(action, payload) {
      var message = '' + action + JSON.stringify(payload) + this._salt;
      return (0, _sha2.default)(message);
    }
  }]);

  return ProsvApiClient;
}();

exports.default = ProsvApiClient;
;