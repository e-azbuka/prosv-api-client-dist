# URL pattern

http://catalog.prosv.ru/api/v1/ebook?action=${action}&consumer=${consumer}&signature=${signature}

All methods are POST.

# Making signature

consumer = azbuka
pattern: %ACTION%%PAYLOAD%%SALT%
algorithm: sha1

signature = sha1(ACTION+PAYLOAD+SALT)

**Since the signature contains action and payload, it must be generated every time you make a request!**

# Known actions

* showcase - returns showcase - identifiers of allowed items for consumer. 
* item - returns item description; Arguments:
    * id: Number, required - id of an item (obtained by showcase request);
    * specifications: Array of string - specifications only what you want to get; Known: name, authors.
* contents - returns contents of item (JSON-data of a book); required argument: id (obtained by showcase request);

# Additional arguments

You can use filters by providing object with these arguments as payload of request:

* specifications: Array - filter by specifications; Known specifications: name, authors.

# Returning result

All methods return status code 200, JSON-body with these required arguments:

* result - contains result of request; null if error occurred;
* error - error description if error occurred; null if everything is ok.


# Examples

http://catalog.prosv.ru/api/v1/ebook?action=showcase&consumer=test&signature=eb2f4b1049e66f74900f4be6dac3a8bce82e8aa3
payload: {}

http://catalog.prosv.ru/api/v1/ebook?action=item&consumer=test&signature=b6f86954920e3a54107bcfbe1b91cb6de33a2bf0
payload: {"id":1984}

http://catalog.prosv.ru/api/v1/ebook?action=item&consumer=test&signature=b58b3c4a4b0c9bb9887e3d199d853fb3d605dcf3
payload: {"id":1984,"specifications":["name","authors"]}

http://catalog.prosv.ru/api/v1/ebook?action=contents&consumer=test&signature=36b188dc5ab52766948baa47f6c83b8f18dc6469
payload: {"id":1984}
